package tron

import (
	"gitee.com/fu-ce-wei/wallet-grpc/core/base"
)

var (
	_ base.Account = (*Account)(nil)
	_ base.Chain   = (*Chain)(nil)
	//_ base.Token       = (*Token)(nil)
	//_ base.Token       = (*Erc20Token)(nil)
	//_ base.Transaction = (*Transaction)(nil)
)
