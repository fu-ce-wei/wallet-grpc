// Code generated by goctl. DO NOT EDIT.

package genModel

import (
	"context"
	"database/sql"
	"fmt"
	"strings"

	"github.com/zeromicro/go-zero/core/stores/builder"
	"github.com/zeromicro/go-zero/core/stores/cache"
	"github.com/zeromicro/go-zero/core/stores/sqlc"
	"github.com/zeromicro/go-zero/core/stores/sqlx"
	"github.com/zeromicro/go-zero/core/stringx"
)

var (
	deviceBasicFieldNames          = builder.RawFieldNames(&DeviceBasic{})
	deviceBasicRows                = strings.Join(deviceBasicFieldNames, ",")
	deviceBasicRowsExpectAutoSet   = strings.Join(stringx.Remove(deviceBasicFieldNames, "`id`", "`create_at`", "`create_time`", "`created_at`", "`update_at`", "`update_time`", "`updated_at`"), ",")
	deviceBasicRowsWithPlaceHolder = strings.Join(stringx.Remove(deviceBasicFieldNames, "`id`", "`create_at`", "`create_time`", "`created_at`", "`update_at`", "`update_time`", "`updated_at`"), "=?,") + "=?"

	cacheIotPlatformDeviceBasicIdPrefix = "cache:iotPlatform:deviceBasic:id:"
)

type (
	deviceBasicModel interface {
		Insert(ctx context.Context, data *DeviceBasic) (sql.Result, error)
		FindOne(ctx context.Context, id int64) (*DeviceBasic, error)
		Update(ctx context.Context, data *DeviceBasic) error
		Delete(ctx context.Context, id int64) error
	}

	defaultDeviceBasicModel struct {
		sqlc.CachedConn
		table string
	}

	DeviceBasic struct {
		Id              int64          `db:"id"`
		CreatedAt       sql.NullTime   `db:"created_at"`
		UpdatedAt       sql.NullTime   `db:"updated_at"`
		DeletedAt       sql.NullTime   `db:"deleted_at"`
		Identity        sql.NullString `db:"identity"`
		ProductIdentity sql.NullString `db:"product_identity"`
		Name            sql.NullString `db:"name"`
		Key             sql.NullString `db:"key"`
		Secret          sql.NullString `db:"secret"`
		LastOnlineTime  sql.NullInt64  `db:"last_online_time"`
	}
)

func newDeviceBasicModel(conn sqlx.SqlConn, c cache.CacheConf) *defaultDeviceBasicModel {
	return &defaultDeviceBasicModel{
		CachedConn: sqlc.NewConn(conn, c),
		table:      "`device_basic`",
	}
}

func (m *defaultDeviceBasicModel) Delete(ctx context.Context, id int64) error {
	iotPlatformDeviceBasicIdKey := fmt.Sprintf("%s%v", cacheIotPlatformDeviceBasicIdPrefix, id)
	_, err := m.ExecCtx(ctx, func(ctx context.Context, conn sqlx.SqlConn) (result sql.Result, err error) {
		query := fmt.Sprintf("delete from %s where `id` = ?", m.table)
		return conn.ExecCtx(ctx, query, id)
	}, iotPlatformDeviceBasicIdKey)
	return err
}

func (m *defaultDeviceBasicModel) FindOne(ctx context.Context, id int64) (*DeviceBasic, error) {
	iotPlatformDeviceBasicIdKey := fmt.Sprintf("%s%v", cacheIotPlatformDeviceBasicIdPrefix, id)
	var resp DeviceBasic
	err := m.QueryRowCtx(ctx, &resp, iotPlatformDeviceBasicIdKey, func(ctx context.Context, conn sqlx.SqlConn, v interface{}) error {
		query := fmt.Sprintf("select %s from %s where `id` = ? limit 1", deviceBasicRows, m.table)
		return conn.QueryRowCtx(ctx, v, query, id)
	})
	switch err {
	case nil:
		return &resp, nil
	case sqlc.ErrNotFound:
		return nil, ErrNotFound
	default:
		return nil, err
	}
}

func (m *defaultDeviceBasicModel) Insert(ctx context.Context, data *DeviceBasic) (sql.Result, error) {
	iotPlatformDeviceBasicIdKey := fmt.Sprintf("%s%v", cacheIotPlatformDeviceBasicIdPrefix, data.Id)
	ret, err := m.ExecCtx(ctx, func(ctx context.Context, conn sqlx.SqlConn) (result sql.Result, err error) {
		query := fmt.Sprintf("insert into %s (%s) values (?, ?, ?, ?, ?, ?, ?)", m.table, deviceBasicRowsExpectAutoSet)
		return conn.ExecCtx(ctx, query, data.DeletedAt, data.Identity, data.ProductIdentity, data.Name, data.Key, data.Secret, data.LastOnlineTime)
	}, iotPlatformDeviceBasicIdKey)
	return ret, err
}

func (m *defaultDeviceBasicModel) Update(ctx context.Context, data *DeviceBasic) error {
	iotPlatformDeviceBasicIdKey := fmt.Sprintf("%s%v", cacheIotPlatformDeviceBasicIdPrefix, data.Id)
	_, err := m.ExecCtx(ctx, func(ctx context.Context, conn sqlx.SqlConn) (result sql.Result, err error) {
		query := fmt.Sprintf("update %s set %s where `id` = ?", m.table, deviceBasicRowsWithPlaceHolder)
		return conn.ExecCtx(ctx, query, data.DeletedAt, data.Identity, data.ProductIdentity, data.Name, data.Key, data.Secret, data.LastOnlineTime, data.Id)
	}, iotPlatformDeviceBasicIdKey)
	return err
}

func (m *defaultDeviceBasicModel) formatPrimary(primary interface{}) string {
	return fmt.Sprintf("%s%v", cacheIotPlatformDeviceBasicIdPrefix, primary)
}

func (m *defaultDeviceBasicModel) queryPrimary(ctx context.Context, conn sqlx.SqlConn, v, primary interface{}) error {
	query := fmt.Sprintf("select %s from %s where `id` = ? limit 1", deviceBasicRows, m.table)
	return conn.QueryRowCtx(ctx, v, query, primary)
}

func (m *defaultDeviceBasicModel) tableName() string {
	return m.table
}
